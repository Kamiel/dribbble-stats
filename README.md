# dribbble-stats

Attendify test code challenge.

## Installation

    lein install

## Usage

    lein run "username"

## License

Beerware © 2017

Distributed under the Beerware license  either version 42 or (at
your option) any later version.

***

## Task #1 ##

> 1. How to deal with Dribbble API Limits?

Calculate timeouts according to headers in each response, described in API.

> 2. What if we have a server that respond to incoming requests instead of CLI tool?

Use something like `ring` to route requests. Also in this case it will be much better
to implement full "Web Application Flow" OAuth for our server to run requests for each user
with unique token.

> 3. What if I want to stop process and restart from a some kind of "savepoint"?

Application should correctly handle http exceptions, also it depend: for in-memory we can use
`memoize`. For server restarting - implement DB (or Hadoop in case of large data)
state saving and loading.

> 4. Futures VS. Async VS. Streaming VS. Deferreds VS. "you-name-it"?

Ask me during on-site interview, I'll be prepeared :sunglasses:

> 5. How to use systems like Storm OR Summingbird OR Spark OR Onyx to solve tasks like this?

In case when we have many threads (it make sense only if we have more than one token
because of rate limit), we can take advantage of parallel processing by replacing
`map` and `reduce` functions by `Spark`'s ones.
