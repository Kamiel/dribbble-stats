(ns dribbble-stats.core
  (:require [clj-http.client :as http]
            [cheshire.core :as json])
  (:gen-class))

;; TODO: store keys external

(def api-client-access-token
  "4a9d529ce0992d5e4da67f96129cd8ff729d8672d4756cfe11383c49154ddc2b")

(def api-client-id
  "f23f367fa1dd3ccf6f0d5acced685c7e9b11eb844eea76a355db7c893f6b3493")

(def api-client-secret
  "a1e58a851574a3b2cc792ed9a00d3a3704bf6a8070cfb8cf4e8bc9f2a7c1d356")

(def base-url
  "Service root path."
  "https://api.dribbble.com/v1/")

(defn parse-body
  "Parse body as json for RESPONSE."
  [response]
  (-> response
      :body
      (json/parse-string true)))

(defn parse-header
  "Read HEADER from RESPONSE."
  [response header]
  (-> response
      :headers
      (get header)
      read-string))

(defn http-get
  "Authorized get request to URL."
  [url]
  (letfn [(http-get-reduce [current-body current-url]
            (let [response (http/get
                            current-url
                            {:oauth-token api-client-access-token})
                  body (concat current-body (parse-body response))
                  next-url (-> response :links :next :href)
                  remaining (parse-header response "X-RateLimit-Remaining")
                  limit (parse-header response "X-RateLimit-Limit")]
              (if next-url
                (let [timeout (numerator (/ (- limit remaining) 60))]
                  (println ; for debug purposes only
                   (format "GET %s  Left: %d, delay: %d"
                           current-url remaining timeout))
                  (Thread/sleep
                   (* timeout 1000))
                  (recur body next-url))
                body)))]
    (http-get-reduce [] url)))
;; TODO: Catch errors

(defn http-get-path
  "Authorized get request to relative PATH."
  [path]
  (http-get (format "%s%s" base-url path)))
;; TODO: use smart concatination url path components

(defn followers-url
  "Followers of USER."
  [username-or-id]
  (format "users/%s/followers" username-or-id))

(defn get-followers
  "Request for followers of USER."
  [user]
  (http-get-path (followers-url user)))

(defn follower-ids
  "Parse follower's ids from FOLLOWERS."
  [followers]
  (map #(-> %
            :follower
            :id)
       (-> followers
           :body
           (json/parse-string true))))

(defn shots-url
  "Shots of USER."
  [username-or-id]
  (format "users/%s/shots" username-or-id))

(defn get-shots
  "Request for shots of USER."
  [username-or-id]
  (http-get-path (shots-url username-or-id)))

(defn shot-ids
  "Parse shot's ids from SHOTS."
  [shots]
  (map :id
       (-> shots
           :body
           (json/parse-string true))))

(defn likes-url
  "Likes for a SHOT."
  [shot-id]
  (format "shots/%s/likes" shot-id))

(defn get-likes
  "Request for likes for a SHOT."
  [shot-id]
  (http-get-path (likes-url shot-id)))

(defn likers
  "Likers user id for LIKES."
  [likes]
  (map #(-> %
            :user
            :id)
       (-> likes
           :body
           (json/parse-string true))))

(defn counted-dict
  "For each element from list as a key, put number of that elements as a value."
  [list]
  (reduce #(assoc % %2 (inc (% %2 0))) {} list))

(defn sort-map-by-values
  "Sort DICTIONARY by values and return it as list of vectors with key and value."
  [dictionary]
  (into (sorted-map-by
         #(compare [(dictionary %2) %2]
                   [(dictionary %) %]))
        dictionary)) ;; TODO: use reduce to keep only few element in memory

(defn grab
  "Grab content for given USER."
  [user]
  (->> user
       get-followers
       follower-ids
       (map get-shots)
       (map shot-ids)
       flatten
       (map get-likes)
       (map likers)
       flatten
       counted-dict
       sort-map-by-values
       (take 10)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (let [given-user (first args)]
    (when (string? given-user)
      (println (str (grab given-user)))))
  (println "Goodbye!"))
