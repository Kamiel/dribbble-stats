(defproject dribbble-stats "0.1.0-SNAPSHOT"
  :description "Attendify test code challenge."
  :url "https://Kamiel@bitbucket.org/Kamiel/dribbble-stats.git"
  :license {:name "Beerware"
            :url "https://gist.githubusercontent.com/Pitometsu/843f60cb09a9146301c58dd4d25490f0/raw/7881efd020f5b481930f76a6ea91e72477d5f71c/Beerware%2520License"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-http "3.3.0"]
                 [cheshire "5.7.0"]]
  :aot :all
  :main dribbble-stats.core
  :target-path "target/%s")
