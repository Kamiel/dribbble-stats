((nil . ((flycheck-disabled-checkers
          . (clojure-cider-typed))
         (eval . (when (require 'projectile)
                   (setq cider-repl-history-file
                         (concat (projectile-project-root)
                                 ".lein-repl-history")))))))
